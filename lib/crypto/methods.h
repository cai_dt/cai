//
//  methods.h
//
//
//  Created by Marcello Fonda on 14/01/16.
//
//

#ifndef ____methods__
#define ____methods__

#include <stdio.h>

void fullAtbash (char *omega);
void caesar (char *omega);

#endif /* defined(____methods__) */
