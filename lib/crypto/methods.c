//
//  methods.c
//
//
//  Created by Marcello Fonda on 14/01/16.
//
//

#include "methods.h"

#include <ctype.h>

void fullAtbash(char *omega)
{
    for(int i = 0; omega[i]; i++)
    {
        omega[i] = (char)159 - omega[i];
    }
}

void caesar (char *omega)
{
    int key = 0;
    printf("Insert key\n");
    scanf("%d", &key);

    for(int i = 0; omega[i]; i++)
    {
        char a = 0;
        if (isupper(omega[i])) {
            a = 'A';
        }
        else if (islower(omega[i]))
        {
            a = 'a';
        }

        if (isalpha(omega[i])) {
            omega[i] = ((omega[i] - a + key) % 25) + a;
        }
    }
}
