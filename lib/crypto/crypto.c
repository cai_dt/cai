#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "crypto.h"
#include "methods.h"

static int password()
{
    int ret = 0;
    printf("Thunder!\n");
    char word[10];
    scanf("%s", word);

    if (!strcmp("Lightning", word)) {
        ret = 1;
    }
    return ret;
}

FILE *inp;
FILE *out;
FILE *err;

static void defineMode(FILE *file, const char *mode)
{
    printf("Shall I use console or file %s?\nType \"console\" or \"file\"\n", mode);

    int irec = 1;
    int invalidInputMode = 1;
    do {
        char input[7];
        scanf("%s", input);
        if(!strcmp(input, "file"))
        {
            invalidInputMode = 0;
            do {
                printf("Please specify a path for the %s file\n", mode);
                char path[32];
                scanf("%s", path);

                if(!strcmp(mode, "input"))
                    file = fopen(path, "r");
                else if(!strcmp(mode, "output"))
                    file = fopen(path, "w");
                else if(!strcmp(mode, "error"))
                    file = fopen(path, "w");
                else {
                    fprintf(err, "ERROR: invalid mode call\n");
                    exit(1);
                }

                if (file != NULL) {
                    printf("File succesfully defined as %s\n", mode);
                    irec = 0;
                }
                else printf("Could not open file\n");
            } while (irec); //Continue asking until you get it right
        }
        else if(!strcmp(input, "console"))
        {
            invalidInputMode = 0;

            if(!strcmp(mode, "input"))
                file = stdin;
            else if(!strcmp(mode, "output"))
                file = stdout;
            else if(!strcmp(mode, "error"))
                    file = stderr;
            else {
                fprintf(err, "ERROR: invalid mode call\n");
                exit(1);
            }

            printf("Console %s selected\n", mode);
            irec = 0;
        }
        else printf("Invalid input mode\n");
    } while (invalidInputMode); //Continue asking until you get it right

}

static int readCommand()
{
    int method = 0;
    char in[20];

    while (!method) {

        scanf("%s", in);
        if (!strcmp("-fullatbash", in)) {
            method = 1;
        }
        else if (!strcmp("-caesar", in)) {
            method = 2;
        }
        else printf("Sorry, but I don't understand\n");
    }
    return method;
}

static void wordEncode (char *delimiter, void (*method)(char*))
{
    printf("Type message:\n");
    char omega[45];
    scanf("%s", omega);
    while (strcmp (omega, delimiter)) {

        (*method) (omega);

        fprintf(out, "%s ", omega);
        scanf("%s", omega);
    }

}

void crypto()
{
    inp = stdin;
    out = stdout;
    err = stderr;

    if (password()) {
        printf("Welcome, my master!\nWhat do you need?\n");

        defineMode(inp, "input");
        defineMode(out, "output");
        defineMode(out, "error");

        char delimiter[10];
        printf("What should I use to delimit end of message?\n");
        scanf("%s", delimiter);

        printf("What kind of encoding would you like to work with?\n");
        switch (readCommand()) {
            case 1:
                printf("Full-atbash encoding selected.\n");
                wordEncode(delimiter, fullAtbash);
                break;
            case 2:
                printf("Caesar encoding selected.\n");
                wordEncode(delimiter, caesar);
            default:
                break;
        }

    }
    else printf("Nothing to see here, go play somewhere else.");
}
