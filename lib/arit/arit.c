/********************************************************************
 * arit.c                                                           *
 *                                                                  *
 * This file contains all functions relative to the -comp command   *
 *                                                                  *
 ********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arit.h"

#define BUFFER_SIZE 31
#define INPUT_SIZE 10

static void intro()
{
    static int count = 0;
    if (count) printf ("Type an operation in RPN; for help, type \"-help\".\n\n");
    else printf ("Type an operation written in Reverse Polish Notation,\nseparating the arguments with spaces.\nType \"=\" to get final result\nor \"-end\" to exit.\nFor help, type \"-help\".\n\n");
    if(count == 0) count++;
}

static void arit (char **c, int *i)
{
    int first_member = 0;
    int second_member = 0;

    /* Since operator is saved in c[*i], use c[*i-2] and c[*i-1]
     * as operands. */
    first_member = atoi(c[*i-2]);
    second_member = atoi(c[*i-1]);

    /* Execute operations */
    if (!strcmp(c[*i], "+")) first_member += second_member;
    else if (!strcmp(c[*i], "-")) first_member -= second_member;
    else if (!strcmp(c[*i], "*") || !strcmp(c[*i], "x")) first_member *= second_member;
    else {
        printf("ERROR: paradoxal call of computing with unrecognised operator\n");
        exit(3);
    }

    sprintf(c[*i-2], "%d", first_member);
    *i-= 2;
}

static void help(int *i);

/*
 * Verify if string c is a command and in case return a value
 * representing that comment
 */
static char readCommand(char *c)
{
    char ret = 'n';
    scanf("%s", c);

    if (!strcmp(c, "+") || !strcmp(c, "-") || !strcmp(c, "*") || !strcmp(c, "x"))
        ret = 'c';
    else if (!strcmp(c, "="))
        ret = '=';
    else if (!strcmp(c, "-help"))
        ret = 'h';
    else if (!strcmp(c, "-end"))
        ret = 'e';
    else if (atoi(c) == 0)
    {
        if (c[0] == '-')
            ret = 'i';
        else if (strcmp(c, "0"))
            ret = 'E';
    }

    return ret;
}

void compute()
{
    int H = 1;


    do{
        intro();

        char **c;    /* Array of strings in which to save inputs */
        if((c = malloc(sizeof(char*) * BUFFER_SIZE)) == NULL)
        {
            printf("ERROR: out of memory\n");
            exit(7);
        }
        for(int i = 0; i < BUFFER_SIZE; i++)
            if((c[i] = malloc(sizeof(char) * INPUT_SIZE)) == NULL)
            {
                printf("ERROR: out of memory\n");
                exit(7);
            }

        int read = 1; /* Tells the program whether it should continue reading input or not */
        int running = 1;
        int right_input = 1;

        do {
            static int i = 0;

            switch (readCommand(c[i])) {
                case 'c':
                    arit(c, &i);
                    if (right_input) printf("Intermediate result is %s\n", c[i]);
                    break;

                case '=':
                    read = 0;
                    break;

                case 'h':
                    help(&i);
                    break;

                case 'e':
                    read = 0;
                    H = 0;
                    running = 0;
                    break;

                case 'i':
                    printf("Invalid command, retype\n\n");
                    right_input = 0;
                    read = 0;
                    i = 0;
                    break;

                case 'E':
                    printf("Invalid input, retype\n\n");
                    right_input = 0;
                    read = 0;
                    i = 0;
                    break;

                default:
                    /* Just store the value, without doing anything */
                    break;
            }

            if(read) i++;
            else i = 0;

        } while(read);

        if(running && right_input) printf("Result is: %s\n\n", c[0]);
        free(c);

    } while(H);
}

static void help(int *i)
{
    (*i)--;
    printf("\n\n\n\
\
Adapted from Wikipedia:\n\n\
Reverse Polish notation (RPN), or postfix notation, is a mathematical notation\n\
in which every operator follows all of its operands.\n\
For instance, to add 3 and 4, one would write \"3 4 +\" rather than \"3 + 4\".\n\
If there are multiple operations, the operator is given immediately after its \n\
second operand; so the expression written \"3 − 4 + 5\" in conventional notation \n\
would be written \"3 4 − 5 +\" in RPN: 4 is first subtracted from 3, then 5 added \n\
to it.\n\
An advantage of RPN is that it removes the need for parentheses that are \n\
required by infix.\n\
While \"3 − 4 × 5\" can also be written \"3 − (4 × 5)\", that means something quite \n\
different from \"(3 − 4) × 5\". In postfix, the former could be written \n\
\"3 4 5 × −\", which unambiguously means \"3 (4 5 ×) −\" which reduces to \"3 20 −\"; \n\
the latter could be written \"3 4 − 5 ×\" (or 5 3 4 − ×, if keeping similar \n\
formatting), which unambiguously means \"(3 4 −) 5 ×\".\n\n\
\
This Interface currently supports the following operators: +, -, * and x \n\
(the two last are interchangeable).\n\
It features a %d-parameters input buffer and each parameter must have a maximum \n\
lenght of %d characters. The parameters should be entered separated from each \n\
other by spaces.\n\
Please note that, when you ask the Interface for the final result, the number of\n\
the operators you entered should be equal to the number of the operands -1 \n\
(since the Interface currently supports only binary operators); \n\
in case of fewer operators you will get one of the intermediate results instead.\n\
This program works with int type variables, so please be nice and kind and don't\n\
try to mess up with numbers over 2147483647 or under -2147483647 ;)\n\n\n", BUFFER_SIZE, INPUT_SIZE);
}
