/**************************************************************************************
 *                                                                                    *
 * CAI - Console Advanced Interface                                                   *
 * Simple and advanced programs on command line interface.                            *
 * A project created by Marcello Fonda and developed by the CAI Developement Team:    *
 * Filippo Costa, Francesco Fornasaro and Marcello Fonda.                             *
 * WIP - Started on November, 6, 2015.                                                *
 * Open source                                                                        *
 *                                                                                    *
 **************************************************************************************/

/**************************************************************************************
 * main.c                                                                             *
 *                                                                                    *
 * Core program, asks user for commands to execute and calls them from other files.   *
 *                                                                                    *
 **************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lib/arit/arit.h"
#include "lib/crypto/crypto.h"

void list();

void intro()
{
    printf ("\nCAI initiated...\n");
    static int count = 0;
    if (count) printf ("Type a command.\nType -ls for a list of available commands\n\n");
    else printf ("Welcome\nThis program is a Work In Progress interface which will include many programs. \nTo start, type '-ls' (without quotes) for a detailed list of available commands.\n\n");
    if (count == 0) count++;
} //Special thanks to Wil.

int readCommand()
{
    char command[8];
    scanf("%s", command);

    int ret = -1;

    if (!strcmp (command, "-end"))
        ret = 0;
    else if (!strcmp (command, "-ls"))
        ret = 1;
    else if (!strcmp (command, "-comp"))
        ret = 2;

    return ret;
}

int main()
{
    for (;;) {

        intro();

        switch (readCommand()) {
            case 0:
                printf("\nEnding program...\n");
                exit(0);
                break;

            case 1:
                printf("\nListing available commands:\n");
                list();
                break;

            case 2:
                printf("\nComputing program started.\n");
                compute();
                break;

            case 3:
                printf("\nWell well well, looks like somebody's gone a little too far...\n");
                crypto();
                break;

            default:
                printf("\nInvalid command\nTry -ls for a list of available commands\n\n");
                break;
        }
    }
    return 0;
}

/*
 * Print a list of available commands for execution
 */
void list()
{
    printf(" -ls \t List of all available commands.\n");
    printf(" -comp \t Start the aritmethic computation program.\n");
    printf(" -end \t Quit the interface.\n");
    printf("\n");
}
