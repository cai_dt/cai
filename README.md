# CAI

> Console Advanced Interface - an open source project founded by Marcello Fonda in 2015.

## Versioning
CAI's version numbers take the form of `MAJOR.MINOR.patch`. The first two's must be expressed with 0-9 digits, while `patch` should stick to hexadecimals. A different codename is also paired with every `MAJOR` update, such as *Melted snow* or *Wise man*.

The latest release is the `Dark Sun 0.1.0`.

### updating
1. Increment `MAJOR` when you make incompatible API changes.
2. Increment `MINOR` when you add backward-compatible functionalities.
3. Increment `patch` when you make bug fixes.

## Install

1. Download the project's folder

>     2. Compile and run:
>         + From Code::Blocks®
>             - Open CAI.cbp
>             - In the menu choose Build > Build and run
>         + From command line:
>             - `cd [path/to/the/project]`
>             - `gcc -o [relative/path/exec_name] [all] [source] [files]`
>             - `./relative/path/exec_name`
>
>* Dependencies
>     - The only dependencies are the `stdio.h`, `stdlib.h` and `string.h` libraries, which should come with the OS

## Community
Wheter you need help, want to keep up with progress, chat with developers, or ask any other questions about the project, you can just send us an email! The `maintainers` section is what you're looking for.

To report bugs, please create a **GIT** issue.

### contribute
If you do think the project could benefit from your contribution, then please go visit our [Bitbucket repository](https://bitbucket.org/cai_dt/cai) for more informations.

## How does it work?
Actually, that's fairly easy: a main function that parses your input strings and passes requests to executables called by the single commands.

And that's it. Basic, right? We know, but it is so on purpose: modularity and flexibility are overall our primary goals for this project.

## Maintainers
- Marcello Fonda <marcellofonda@gmail.com>
- Francesco Fornasaro <francesco.fornasaro11@gmail.com>
- Filippo Costa <filippocosta.italy@gmail.com>